import pywikibot
from pywikibot import pagegenerators as pg
import os
from pywikibot.data import api

class pyWDHelper:

    def __init__(self, siteStr="wikidata", familyStr="test"):

        self.site = pywikibot.Site(familyStr, siteStr)
        self.repo = self.site.data_repository()

    def getItem(self, qid):
        item = pywikibot.ItemPage(self.repo, qid)
        return item
        #return item.get()
    
    def setLabels(self, item, labelsDict, summary="Setting labels"):
        #labelsDict = {"de": "test", "nl": "testwijz"}
        item.editLabels(labels=labelsDict, summary=summary)

    def setQualifer(self, claim, prop, value, summary=u'Adding a qualifier reason '):
        #QUALIFIER
        qualifier = pywikibot.Claim(self.repo, prop) #u'P90196'
        target = pywikibot.ItemPage(self.repo, value) # "Q215410"
        qualifier.setTarget(target)
        claim.addQualifier(qualifier)

    def changeRankWithValueCheck(self, item, prop, claimValue, summary="Set Rank"):
        item.get()
        if (item.claims[prop]):
            for claim in item.claims[prop]:
                print(claim)
                if claim.target == claimValue:
                    rank = claim.getRank()
                    if rank == "normal":
                        print("set to deprecated")
                        print(claim.changeRank("deprecated"))
                        return claim
    
    def addStringClaim(self, item, prop, value, summary="Adding a new value"):
        #Add new Url 
        #newUrl = "https://www.merkur-zeitschrift.de/artikel/eine-frau-ein-goldhamster-und-zwei-walnuesse-a-mr-71-1-98/"
        valueAlreadySet = False
        if (item.claims[prop]):
            for claim in item.claims[prop]:
                print(claim)
                if claim.target == value:
                    valueAlreadySet = True
        if valueAlreadySet == False:
            stringclaim = pywikibot.Claim(self.repo, prop) #u'P98074' #Else, add the value
            stringclaim.setTarget(value)
            item.addClaim(stringclaim, summary) #=u'Adding a new URL'
            return stringclaim
        else:
            return None

    def create_item(self, label_dict, desc_dict, summary="Creating a new item."):
        new_item = pywikibot.ItemPage(self.site)
        new_item.editLabels(labels=label_dict, summary=f"{summary} Setting labels")
        new_item.editDescriptions(descriptions=desc_dict, summary=f"{summary} Setting Descriptions")
        return new_item.getID()
    
    def addStatement(self, qid, property, target, summary="Adding Statement"):
        item = pywikibot.ItemPage(self.repo, qid) #Make the WD-Item (Subject) editable
        claim = pywikibot.Claim(self.repo, property) #Add the Property to the Item
        target = pywikibot.ItemPage(self.repo, target) #Add an Object (QID) to the Item
        claim.setTarget(target) #Set the target value in the local object.
        item.addClaim(claim, summary=f'{summary}') #Inserting value with summary to Q210194
        return claim

    def addStringStatement(self, qid, property, value, summary="Adding Statement"):
        item = pywikibot.ItemPage(self.repo, qid)
        stringclaim = pywikibot.Claim(self.repo, property) 
        stringclaim.setTarget(value)
        item.addClaim(stringclaim, summary=f'{summary}')
        return stringclaim

    def addQualifier(self, claim, property, target, summary="Adding a qualifier "):
        qualifier = pywikibot.Claim(self.repo, property)
        target = pywikibot.ItemPage(self.repo, target)
        qualifier.setTarget(target)
        claim.addQualifier(qualifier, summary=f'{summary}')
    
    def addDateStatement(self, qid, property, dateDict, summary="Adding date value"):
        item = pywikibot.ItemPage(self.repo, qid)
        dateclaim = pywikibot.Claim(self.repo, property)
        dateValue = pywikibot.WbTime(year=dateDict["year"], month=dateDict["month"], day=dateDict["day"])
        dateclaim.setTarget(dateValue)
        item.addClaim(dateclaim, summary=f'{summary}')
        return dateclaim

    def addMonoLingStatement(self, qid, property, string, lang, summary="Adding Statement"):
        item = pywikibot.ItemPage(self.repo, qid)
        stringclaim = pywikibot.Claim(self.repo, property) 
        monoLingString = pywikibot.WbMonolingualText(string, lang)
        stringclaim.setTarget(monoLingString)
        item.addClaim(stringclaim, summary=f'{summary}')
        return stringclaim
    
    def checkWikiitemexists(self, label, lang):
        params = {'action': 'wbsearchentities', 'format': 'json',
                'language': lang, 'type': 'item', 'limit':1,
                'search': label}
        request = api.Request(self.site, parameters=params)
        result = request.submit()
        return True if len(result['search'])>0 else False
    
if __name__ == '__main__':
    os.environ['PYWIKIBOT_DIR'] = './'
    wd = pyWDHelper()
    some_labels = {"en": "Mountains Digitized", "de": "Mountains digitzied"}
    some_descs = { "de": "Artikel in Syntopia (2024)"}
    qid = "Q233762"
    #new_item_id = wd.create_item(some_labels, some_descs)
    #print(new_item_id)
    #wd.addStatement("Q233762", "P97010", "Q227265")
    #wd.addStringStatement("Q233762", "P80807", "Reto Bürgin")
    #claim = wd.addStringStatement("Q233762", "P98074", "https://www.syntopia-alpina.ch/beitraege/zwischen-stadt-und-berg-digitale-multilokalitaet")
    #wd.addQualifier(claim, "P77090", "Q348")
    #wd.addDateStatement(qid, "P761", dateDict={"year":2024, "month": 1, "day":24})
    #wd.addMonoLingStatement(qid,"P77107","Digitalität in den Bergen","de")
    print(wd.checkWikiitemexists("Mountains Digitized","de"))
    