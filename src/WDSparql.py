# pip install sparqlwrapper
# https://rdflib.github.io/sparqlwrapper/

import sys
from SPARQLWrapper import SPARQLWrapper, JSON

endpoint_url = "https://query.wikidata.org/sparql"

def getQuery(url):
    query = f"SELECT * WHERE {{?item wdt:P953 <{url}>}}"
    return query


def get_results(query, endpoint_url="https://query.wikidata.org/sparql"):
    user_agent = "WDQS driven by User:Mfchris84 searching for articles from Syntopia Magazine (CH)/%s.%s" % (sys.version_info[0], sys.version_info[1])
    #adjust user agent; see https://w.wiki/CX6
    sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()


if __name__ == "__main__":
    query = getQuery("https://www.syntopia-alpina.ch/beitraege/experimente-der-natur-warum-der-klimawandel-alpine-pflanzen-weniger-stoert-als-viele-glauben")
    results = get_results(query=query, endpoint_url=endpoint_url)

    for result in results["results"]["bindings"]:
        print(result)
