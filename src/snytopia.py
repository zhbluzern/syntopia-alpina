import requests
from lxml import etree

def getSyntopiaRecords():
    headers = {"Accept": "application/xml"}  
    rssUrl = "https://www.syntopia-alpina.ch/feed.rss"

    r = requests.get(url=rssUrl, headers=headers)

    root = etree.fromstring(r.content)

    items = root.xpath(".//item")
    return items

if __name__ == "__main__":
    items = getSyntopiaRecords()
    for item in items:
        title = item.xpath(".//title")
        print(title[0].text)