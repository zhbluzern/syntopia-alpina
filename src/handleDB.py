import sqlite3 as db
import pandas as pd
import json

class sqldb:

    # Initialize sqlark-Class
    def __init__(self, databasePath):
        self.con = db.connect(databasePath)
    
    def selectByKeyValue(self, key, value):
        cur = self.con
        sql = f"SELECT * FROM t_articles WHERE json_extract(metadata, '$.{key}') = ?"
        res = cur.execute(sql, (value,))  
        rows = res.fetchall()
        return rows

    def storeArticle(self, metadata):
        cur = self.con
        data = json.dumps(metadata)
        sql = "INSERT  OR IGNORE INTO t_articles (metadata) VALUES (?)"
        cur.execute(sql,(data, ))
        self.con.commit()

    def updateArticle(self, metadata, guid):
        cur = self.con
        data = [json.dumps(metadata), guid]
        sql = "UPDATE t_articles SET metadata = ? WHERE json_extract(metadata, '$.guid') = ?"
        cur.execute(sql, data)
        self.con.commit()

    def export2CSV(self, csv_file_path="dbExport.csv"):
        sql = "SELECT * FROM t_articles;"
        db_df = pd.read_sql_query(sql, self.con)
        db_df.to_csv(csv_file_path, index=False)

    def resetTable(self, tablename):
        # Delete all rows from the table
        cur = self.con
        cur.execute(f'DELETE FROM {tablename};')
        cur.execute(f"DELETE FROM sqlite_sequence WHERE name='{tablename}';")
        self.con.commit()
        cur.execute('VACUUM;')
        self.con.commit()

if __name__ == "__main__":
    db = sqldb("database/syntopia.sqlite")
    db.resetTable("t_articles")
    #rows = db.selectByKeyValue("guid", "4859A")
    #print(len(rows))