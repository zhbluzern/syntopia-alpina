import src.snytopia as syntopia
import src.WDSparql as wdQuery
import src.handleDB as db
import src.pyWD as pyWD
import os
from datetime import datetime
import re

def convertDateTimeString(dateTimeString):
# Given date/time string
    #dateTimeString = "Fri, 08 Apr 2022 10:58:00 +0200"
    date_object = datetime.strptime(dateTimeString, "%a, %d %b %Y %H:%M:%S %z")
    result = {}
    result["date"] = date_object
    result["year"] = date_object.year
    result["month"] = date_object.month
    result["day"] = date_object.day
    return result

os.chdir("syntopia-alpina")
items = syntopia.getSyntopiaRecords()
db = db.sqldb("database/syntopia.sqlite")

os.environ['PYWIKIBOT_DIR'] = './'
wd = pyWD.pyWDHelper(familyStr="wikidata")

for item in items:
    resultDet = {}
    #Parse Metadata from Syntopia-RSS-Feed
    resultDet["title"] = item.xpath(".//title")[0].text
    resultDet["pubDate"] = item.xpath(".//pubDate")[0].text
    resultDet["author"] = item.xpath(".//author")[0].text
    resultDet["guid"] = item.xpath(".//guid")[0].text
    resultDet["description"] = item.xpath(".//description")[0].text
    resultDet["url"] = item.xpath(".//link")[0].text
    pubDate = convertDateTimeString(resultDet["pubDate"])

    #Check WD-Item by article URL
    query = wdQuery.getQuery(resultDet["url"])
    results = wdQuery.get_results(query)
    itemExists = wd.checkWikiitemexists(resultDet["title"],"de")
    if results["results"]["bindings"] == [] and itemExists == False:
        print(f"WD-Item needed for {resultDet['title']}!")
        #Create WD-item with pywikibot
        some_labels = {"en": resultDet["title"], "de": resultDet["title"]}
        some_descs = { "en": f"article by {resultDet['author']}  published in Syntopia Alpina ({pubDate['year']})", "de": f"Artikel von {resultDet['author']} veröffentlicht im Online-Magazin Syntopia Alpina ({pubDate['year']})"}
        resultDet["qid"] = wd.create_item(some_labels, some_descs, summary="Creating a new item for an article in Syntopia Alpina")
        print(f"New Item created: {resultDet['qid']}")
        wd.addStatement(resultDet["qid"], "P31", "Q191067") # P31 -> Article
        wd.addStringStatement(resultDet["qid"], "P2093", resultDet["author"], summary="adding author name") # P2093 -> Author Name String
        claim = wd.addStringStatement(resultDet["qid"], "P953", resultDet["url"], summary="adding URL to full work") # P953 -> Full Work URL
        wd.addQualifier(claim, "P407", "Q188") # P953 - LanguageQualifier (set German as default)
        wd.addDateStatement(resultDet["qid"], "P577", dateDict={"year":pubDate["year"], "month": pubDate["month"], "day":pubDate["day"]}, summary="adding publication date") # P577 PublicationDate
        wd.addMonoLingStatement(resultDet["qid"],"P1476",resultDet["title"],"de", summary="adding article title") # Article Title (DE as default Language Code)
        wd.addStatement(resultDet["qid"], "P407", "Q188") # P407 -> Language of Work (German as default)
        wd.addStatement(resultDet["qid"], "P1433", "Q112206291") # P1433 -> Published in Syntopia Alpina
        wd.addStatement(resultDet["qid"], "P6216", "Q50423863") # P6216 copyright status -> copyrighted
    else:
        for result in results["results"]["bindings"]:
            print(f"WD-Item exists:{result['item']['value']}")
            resultDet["qid"] = re.sub("http://www.wikidata.org/entity/","",result["item"]["value"])
    
    #store data in sqlite.database
    print(resultDet)
    rows = db.selectByKeyValue("guid", resultDet['guid'])
    if len(rows) == 0:
        db.storeArticle(resultDet)
    else:
        print(f"data already stored in database :: update entry with rss:Guid={resultDet['guid']}")
        #update -> maybe qid has a new value
        db.updateArticle(resultDet, resultDet["guid"])
    
    #break