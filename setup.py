import os
import sqlite3
import subprocess
import sys


def main():
    # Set default database name
    default_db_name = "articles.sqlite"

    # Prompt for the database filename with default name
    db_name = input(f"Please enter the filename for the SQLite database (default: {default_db_name}): ") or default_db_name
    # Check if the filename has an extension, append '.sqlite' if not
    if '.' not in db_name:
        db_name += ".sqlite"

    # Create path for the database directory
    db_dir = os.path.join(os.getcwd(), 'database')
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)
    # Create database and table
    db_path = f"{db_dir}/{db_name}"
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE t_articles (  metadata TEXT PRIMARY KEY);")
    conn.commit()
    conn.close()

if __name__ == "__main__":
    main()
