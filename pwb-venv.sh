#!/bin/bash
# Install Pywikibot in a virtual environment from git sources
# (Pywikibot's PyPi package does not include scripts)

# clean up directories if they already exist
rm -fdr pwbvenv $HOME/pywikibot-core

# create a new virtual environment
python3 -m venv pwbvenv

# activate it
source pwbvenv/bin/activate

# clone Pywikibot
git clone --recursive --branch stable "https://gerrit.wikimedia.org/r/pywikibot/core" $HOME/pywikibot-core

# install dependencies
pip install --upgrade pip setuptools wheel
pip install $HOME/pywikibot-core[mwoauth,mysql]  # update as needed
# pip install ... # any other dependencies here
pip install lxml
pip install sparqlwrapper
pip install pandas
